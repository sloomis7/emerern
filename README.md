# Emere React Native

This software is a portfolio application intended to demostrate my professional capabilities in React Native Development.

It's function is a e-commerce app that sells general home goods.

This app is built in React Native and Typescript.

It uses a remote backend already deployed @ heroku.com

The Backend is in Go and it uses a MongoDB Database

If you have trouble getting the app to run please email sloomisemployment@gmail.com for help

Mock Data was custom built from Wayfair.com 

## Features

- Working user login: Authentication with JWT tokens
- User Sign up / Sign in
- Auto sign in if token is valid
- Home Screen
- Product Details Screen

## Installation

Install react native and yarn
https://reactnative.dev/docs/0.60/enviroment-setup

```sh
npm install -g react-native-cli
```
Make sure command line tools is installed for IOS 
Install React Native tools in VSCode

Install Yarn
https://classic.yarnpkg.com/en/docs/install/#mac-stable

In VSCode Project Root:

```sh
npm install
yarn start
yarn run is
cd ios && pod install && cd ..
```

Android:
Follow react native install for Android And
Make sure Tools -> SDK Manager -> Android SDK -> SDK Tools -> Android Emulator AND Android Platform SDK Tools are updated


```sh
export ANDROID_HOME=/Users/{yourusername}/Library/Android/sdk
export PATH=$ANDROID_HOME/platform-tools:$PATH
export PATH=$ANDROID_HOME/tools:$PATH
```

Make sure AppData.tsx -> env = Enviroment.dev for local OR Environment.prod for remote
WSManager.tsx: set localhost property to <local network IP address>

