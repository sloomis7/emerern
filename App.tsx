import React from 'react';
import {useEffect} from 'react';

//import Navigator from "./app/routes/MainStack";
import SplashScreen from 'react-native-splash-screen';
import {NavigationContainer} from '@react-navigation/native';
import RootStack from './app/navigation/RootStack';


export default function App() {
  useEffect(() => {
    SplashScreen.hide();
  });

  return (
    <NavigationContainer>
      <RootStack></RootStack>
    </NavigationContainer>
  );
}
