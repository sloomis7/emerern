import React from 'react';
import {Animated, StyleSheet, View, Dimensions, Text} from 'react-native';
import colors from '../../../config/colors';
import AnimatedImage from '../../image/AnimatedImage';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import fonts from '../../../config/fonts';
import Category, {Product} from '../../../models/Category';

interface NewArrivalProps {
  x: Animated.Value;
  index: number;
  product: Product;
  loading: boolean;
  selectionHandler: (ID: string) => void;
}

const NewArrivalView = ({
  index,
  product,
  x,
  loading,
  selectionHandler,
}: NewArrivalProps) => {
  const fullWidth = cardWidth + cardMargin;
  const position = Animated.subtract(index * fullWidth, x);
  const isDisappearing = -fullWidth;
  const isLeft = 0;
  const isRight = windowWidth - fullWidth;
  const isAppearing = windowWidth;

  const translateX = Animated.subtract(
    x,
    x.interpolate({
      inputRange: [0, fullWidth],
      outputRange: [0, fullWidth],
    }),
  );

  const scale = position.interpolate({
    inputRange: [isDisappearing, isLeft, isRight, isAppearing],
    outputRange: [0.85, 1, 1, 0.85],
  });

  return (
    <View>
      <Animated.View
        style={[styles.newArrivalView, {transform: [{translateX}, {scale}]}]}
        key={index}>
        {loading == false ? (
          <SkeletonPlaceholder>
            <View style={styles.newArrivalViewSkel}></View>
          </SkeletonPlaceholder>
        ) : (
          <View style={styles.shadowBackground}>
            <AnimatedImage
              defaultImageSource={require('../../../assets/default.png')}
              source={{
                uri: product.imageUrl,
              }}
              style={styles.categoryImage}
              selectionHandler={selectionHandler}
              ID={product.id}
            />
          </View>
        )}
      </Animated.View>

      {loading == false ? (
        <View style={styles.newArrivalFooter}>
          <View style={styles.footer1}>
            <SkeletonPlaceholder>
              <View style={styles.footerText1}></View>
              <View style={styles.footerText2}></View>
              <View style={styles.footerText3}></View>
            </SkeletonPlaceholder>
          </View>
          <Text numberOfLines={2} style={styles.newArrivalFooterText}>
            {'\n'}
          </Text>
          <Text numberOfLines={1} style={styles.newArrivalFooterPriceText}>
            {' '}
          </Text>
        </View>
      ) : (
        <View style={styles.newArrivalFooter}>
          <Text numberOfLines={2} style={styles.newArrivalFooterText}>
            {product.name.toUpperCase()}
          </Text>
          <Text numberOfLines={1} style={styles.newArrivalFooterPriceText}>
            {product.price}
          </Text>
        </View>
      )}
    </View>
  );
};

const windowWidth = Dimensions.get('window').width;
const cardWidth = windowWidth * 0.7;
const cardMargin = 15;

const styles = StyleSheet.create({
  newArrivalView: {
    width: cardWidth,
    height: cardWidth,
    backgroundColor: colors.white,
    marginHorizontal: cardMargin / 2,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    //overflow: 'hidden',
  },
  categoryImage: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
    borderRadius: 10,
  },
  newArrivalViewSkel: {
    width: cardWidth,
    height: cardWidth,
    borderRadius: 10,
  },
  shadowBackground: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
    shadowColor: colors.black,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.4,
    shadowRadius: 8,
    elevation: 10,
    backgroundColor: colors.white,
    borderRadius: 10,
  },
  newArrivalFooter: {
    //backgroundColor: 'blue',
    width: cardWidth,
    //height: newArrivalsHeight * 0.125,
    alignItems: 'center',
    //justifyContent: 'flex-end',
  },
  newArrivalFooterText: {
    textAlign: 'center',
    fontFamily: fonts.primary_bold,
    fontSize: 15,
    color: colors.wordGray5,
    marginTop: 30,
    marginBottom: 5,
    width: '80%',
  },
  newArrivalFooterPriceText: {
    textAlign: 'center',
    fontFamily: fonts.primary_bold,
    fontSize: 15,
    color: colors.lineGray5,
    marginBottom: 10,
    //marginTop: 20,
    width: '80%',
  },
  footer1: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    alignItems: 'center',
    //justifyContent: 'flex-end',
    //backgroundColor: 'blue',
  },
  footerText1: {
    alignSelf: 'center',
    width: 200,
    height: 14,
    marginBottom: 3,
    marginTop: 30,
  },
  footerText2: {
    alignSelf: 'center',
    width: 200,
    height: 14,
    marginBottom: 4,
  },
  footerText3: {
    alignSelf: 'center',
    width: 75,
    height: 14,
    marginBottom: 10,
  },
});

export default NewArrivalView;
