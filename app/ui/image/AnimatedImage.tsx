import React from 'react';
import {Animated, StyleSheet, TouchableWithoutFeedback} from 'react-native';
import {LoadingState} from '../../util/globalEnum';

interface ImageProps {
  defaultImageSource: any;
  source: any;
  style: any;
  selectionHandler?: (ID: string) => void;
  ID?: string;
}
export default class AnimatedImage extends React.Component<ImageProps> {
  state = {
    image: this.props.defaultImageSource,
    loadingState: LoadingState.loading,
  };

  componentDidMount() {
    this.setState({image: this.props.source});
  }

  loadFallback() {
    this.setState({loadingState: LoadingState.loaded});
    this.setState({image: this.props.defaultImageSource});
  }
  imageAnimated = new Animated.Value(0);

  handleImageLoad = () => {
    this.setState({loadingState: LoadingState.loaded});
    Animated.timing(this.imageAnimated, {
      toValue: 1,
      useNativeDriver: true,
    }).start();
  };

  didSelect = () => {
    if (this.state.loadingState != LoadingState.loading) {
      this.props.selectionHandler?.(this.props.ID ?? "");
    }
  };

  render() {
    const {...props} = this.props;

    return (
      <TouchableWithoutFeedback onPress={this.didSelect}>
        <Animated.Image
          {...this.props}
          source={this.state.image}
          style={[
            this.props.style,
            {opacity: this.imageAnimated},
            styles.imageOverlay,
          ]}
          defaultSource={this.props.defaultImageSource}
          onLoad={this.handleImageLoad}
          onError={() => this.loadFallback()}
        />
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  imageOverlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
});
