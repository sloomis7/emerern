import React from 'react';
import Category, {Product} from '../../../models/Category';
import {StyleSheet, Text, View, Dimensions} from 'react-native';
import fonts from '../../../config/fonts';
import colors from '../../../config/colors';
import AnimatedImage from '../../image/AnimatedImage';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

interface Props {
  category: Category;
  loading: boolean;
  selectionHandler?: (ID: string) => void;
}
const HomeCategoryView = ({category, loading, selectionHandler}: Props) => {
  return (
    <>
      {category == null ? (
        <View></View>
      ) : (
        <View style={styles.background}>
          {loading == true ? (
            <>
              <View style={skeletonStyles.header1}>
                <SkeletonPlaceholder>
                  <View style={skeletonStyles.headerText1}></View>
                </SkeletonPlaceholder>
              </View>
              <Text style={styles.headerText}> </Text>
            </>
          ) : (
            <Text style={styles.headerText}>{category.name}</Text>
          )}

          <View style={styles.mainView}>
            {category.products.map((object, i) => {
              if (loading) {
                return (
                  <View key={i.toString()} style={styles.productView}>
                    <SkeletonPlaceholder>
                      <View style={skeletonStyles.productImageView}></View>
                    </SkeletonPlaceholder>
                    <View style={skeletonStyles.footer1}>
                      <SkeletonPlaceholder>
                        <View style={skeletonStyles.footerText1}></View>
                        <View style={skeletonStyles.footerText2}></View>
                      </SkeletonPlaceholder>
                    </View>
                  </View>
                );
              } else {
                return (
                  <View key={i.toString()} style={styles.productView}>
                    <View style={styles.productImageView}>
                      <AnimatedImage
                        defaultImageSource={require('../../../assets/default.png')}
                        source={{
                          uri: object.imageUrl,
                        }}
                        style={styles.image}
                        selectionHandler={selectionHandler}
                        ID={object.id}
                      />
                    </View>
                    <Text
                      numberOfLines={1}
                      style={styles.productFooterPriceText}>
                      {object.price}
                    </Text>
                    <Text numberOfLines={1} style={styles.productFooterText}>
                      {object.name}
                    </Text>
                  </View>
                );
              }
            })}
          </View>
        </View>
      )}
    </>
  );
};

const windowWidth = Dimensions.get('window').width;
const margin = 15;
const imageViewWidth = windowWidth / 2;

const styles = StyleSheet.create({
  background: {
    width: '100%',
    backgroundColor: colors.white,
  },

  headerText: {
    fontFamily: fonts.primary_bold,
    fontSize: 25,
    color: colors.wordGray5,
    margin: margin,
  },
  mainView: {
    width: '100%',
    //backgroundColor: 'pink',
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingLeft: margin / 2,
  },
  productView: {
    borderRadius: 10,
    marginHorizontal: margin / 2,
    paddingVertical: margin,
    //backgroundColor: 'red',
  },
  productImageView: {
    width: imageViewWidth - margin * 1.5,
    height: imageViewWidth - margin * 1.5,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  image: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
  },
  productFooterPriceText: {
    fontFamily: fonts.primary_bold,
    fontSize: 15,
    color: colors.wordGray5,
    marginTop: 10,
    marginBottom: 10,
    width: imageViewWidth - margin * 1.5,
    //backgroundColor: 'pink',
  },
  productFooterText: {
    fontFamily: fonts.primary_bold,
    fontSize: 15,
    color: colors.lineGray5,
    marginBottom: 10,
    width: imageViewWidth - margin * 1.5,
  },
});

const skeletonStyles = StyleSheet.create({
  header1: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
  },
  headerText1: {
    width: 200,
    height: 23,
    margin: margin,
  },
  productImageView: {
    width: imageViewWidth - margin * 1.5,
    height: imageViewWidth - margin * 1.5,
    borderRadius: 10,
  },
  footer1: {
    // position: 'absolute',
    // top: 0,
    // right: 0,
    // left: 0,
    // bottom: 0,
  },
  footerText1: {
    width: 50,
    height: 16,
    //marginLeft: margin,
    marginTop: 10,
    marginBottom: 10,
  },
  footerText2: {
    width: imageViewWidth - margin * 1.5 - 10,
    height: 16,
    //marginLeft: margin,
    marginBottom: 10,
  },
});

export default HomeCategoryView;
