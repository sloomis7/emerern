import React from 'react';
import Category, {Product} from '../../../models/Category';
import {StyleSheet, Text, View, Dimensions} from 'react-native';
import fonts from '../../../config/fonts';
import colors from '../../../config/colors';
import {FlatList} from 'react-native-gesture-handler';
import AnimatedImage from '../../image/AnimatedImage';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

interface Props {
  category: Category;
  loading: boolean;
  selectionHandler?: (ID: string) => void;
}
const CategoryView = ({category, loading, selectionHandler}: Props) => {
  return (
    <>
      {category == null ? (
        <View></View>
      ) : (
        <View style={styles.background}>
          {loading == true ? (
            <>
              <View style={skeletonStyles.header1}>
                <SkeletonPlaceholder>
                  <View style={skeletonStyles.headerText1}></View>
                </SkeletonPlaceholder>
              </View>
              <Text style={styles.headerText}> </Text>
            </>
          ) : (
            <Text style={styles.headerText}>{category.name}</Text>
          )}

          <FlatList
            contentContainerStyle={styles.flatList}
            keyExtractor={(item: Product) => item.id}
            data={category.products}
            showsHorizontalScrollIndicator={false}
            numColumns={2}
            scrollEnabled={!loading}
            renderItem={({item}) => (
              <View style={styles.productView}>
                {loading == true ? (
                  <>
                    <SkeletonPlaceholder>
                      <View style={skeletonStyles.productImageView}></View>
                    </SkeletonPlaceholder>
                    <View style={skeletonStyles.footer1}>
                      <SkeletonPlaceholder>
                        <View style={skeletonStyles.footerText1}></View>
                        <View style={skeletonStyles.footerText2}></View>
                      </SkeletonPlaceholder>
                    </View>
                  </>
                ) : (
                  <>
                    <View style={styles.productImageView}>
                      <AnimatedImage
                        defaultImageSource={require('../../../assets/default.png')}
                        source={{
                          uri: item.imageUrl,
                        }}
                        style={styles.image}
                        selectionHandler={selectionHandler}
                        ID={category.id}
                      />
                    </View>
                    <Text
                      numberOfLines={1}
                      style={styles.productFooterPriceText}>
                      {item.price}
                    </Text>
                    <Text numberOfLines={1} style={styles.productFooterText}>
                      {item.name}
                    </Text>
                  </>
                )}
              </View>
            )}
          />
        </View>
      )}
    </>
  );
};

const windowWidth = Dimensions.get('window').width;
const margin = 15;
const imageViewWidth = windowWidth / 2;

const styles = StyleSheet.create({
  background: {
    width: '100%',
    backgroundColor: colors.white,
  },

  headerText: {
    fontFamily: fonts.primary_bold,
    fontSize: 25,
    color: colors.wordGray5,
    margin: margin,
  },

  flatList: {
    backgroundColor: colors.white,
    paddingHorizontal: margin / 2,
  },
  productView: {
    marginHorizontal: margin / 2,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
    paddingVertical: margin,
  },
  productImageView: {
    width: imageViewWidth - margin * 1.5,
    height: imageViewWidth - margin * 1.5,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  image: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
  },
  productFooterPriceText: {
    //textAlign: 'center',
    fontFamily: fonts.primary_bold,
    fontSize: 15,
    color: colors.wordGray5,
    marginTop: 10,
    marginBottom: 10,
    width: imageViewWidth - margin * 1.5,
    //backgroundColor: 'pink',
  },
  productFooterText: {
    //textAlign: 'center',
    fontFamily: fonts.primary_bold,
    fontSize: 15,
    color: colors.lineGray5,
    marginBottom: 10,
    //marginTop: 20,
    width: imageViewWidth - margin * 1.5,
  },
});

const skeletonStyles = StyleSheet.create({
  header1: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
  },
  headerText1: {
    width: 200,
    height: 23,
    margin: margin,
  },
  productImageView: {
    width: imageViewWidth - margin * 1.5,
    height: imageViewWidth - margin * 1.5,
    borderRadius: 10,
  },
  footer1: {
    // position: 'absolute',
    // top: 0,
    // right: 0,
    // left: 0,
    // bottom: 0,
  },
  footerText1: {
    width: 50,
    height: 16,
    //marginLeft: margin,
    marginTop: 10,
    marginBottom: 10,
  },
  footerText2: {
    width: imageViewWidth - margin * 1.5 - 10,
    height: 16,
    //marginLeft: margin,
    marginBottom: 10,
  },
});

export default CategoryView;
