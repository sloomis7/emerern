import React, {useState, useRef} from 'react';
import {StyleSheet, Dimensions, FlatList, View} from 'react-native';
import AnimatedImage from '../../../ui/image/AnimatedImage';
import {ImageURL} from '../../../models/Product';
import {ScrollView} from 'react-native-gesture-handler';

const windowWidth = Dimensions.get('window').width;

interface Props {
  dataSource: ImageURL[];
  didScrollHandler: (index: number) => void;
}

const ImageScrollView = ({dataSource, didScrollHandler}: Props) => {
  //hook state for content offset
  //const [constenOffsetX, setConstenOffsetX] = useState(windowWidth);
  const scrollViewRef = useRef(null);

  return (
    <FlatList
      ref={scrollViewRef}
      horizontal={true}
      keyExtractor={(item: ImageURL) => {
        return item.id;
      }}
      data={dataSource}
      showsHorizontalScrollIndicator={false}
      snapToInterval={windowWidth}
      scrollEnabled={dataSource.length > 2 ? true : false}
      decelerationRate={0}
      bounces={false}
      contentOffset={{
        x: windowWidth,
        y: 0,
      }}
      onMomentumScrollEnd={(e) => {
        let offset = e.nativeEvent.contentOffset.x;
        var index = Math.round(offset / windowWidth); // your cell height
        //console.log('now index is ' + index);

        var contentOffSet = 0;
        index -= 1;
        if (index == dataSource.length - 2) {
          index = 0;
          contentOffSet = windowWidth;
        } else if (index == -1) {
          index = dataSource.length - 3;
          contentOffSet = windowWidth * (index + 1);
        }
        didScrollHandler(index);
        if (contentOffSet != 0) {
          let ref = (scrollViewRef.current as unknown) as FlatList;
          ref.scrollToOffset({
            offset: contentOffSet,
            animated: false,
          });
        }
      }}
      renderItem={({item}) => (
        <View style={styles.imageView}>
          <AnimatedImage
            defaultImageSource={require('../../../assets/default.png')}
            source={{
              uri: item.url,
            }}
            style={styles.image}
          />
        </View>
      )}
    />
  );
};

const styles = StyleSheet.create({
  imageView: {
    width: windowWidth,
    height: windowWidth,
  },
  image: {
    width: windowWidth,
    height: windowWidth,
  },
});

export default ImageScrollView;
