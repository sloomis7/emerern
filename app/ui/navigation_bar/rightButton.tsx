import React from 'react';
import {Image, TouchableWithoutFeedback, StyleSheet} from 'react-native';

export default function RightButton(navigation: any) {
  const back = () => {
    //navigation.goBack();
  };
  return (
    <TouchableWithoutFeedback onPress={back}>
      <Image
        style={styles.buttonImage}
        source={require('../../assets/cart.png')}
      />
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  buttonImage: {
    height: 30,
    width: 30,
    marginRight: 15,
  },
});
