import React from 'react';
import {Image, TouchableWithoutFeedback, StyleSheet} from 'react-native';

interface Props {
  navigation: any;
  isDown?: boolean;
}

export default function LeftButton({navigation, isDown}: Props) {
  const back = () => {
    navigation.goBack();
  };
  let style1: any = isDown
    ? [styles.buttonImage, styles.rotate]
    : styles.buttonImage;

  return (
    <TouchableWithoutFeedback onPress={back}>
      <Image style={style1} source={require('../../assets/left_chevron.png')} />
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  buttonImage: {
    height: 30,
    width: 30,
    marginLeft: 5,
  },
  rotate: {
    transform: [{rotate: '270deg'}],
  },
});
