import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
//import colors from '../../config/colors';

export default function Header() {
  return (
    <View style={styles.header}>
      <View>
        <Text style={styles.headerText}>Test</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    flex: 1,
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    //backgroundColor: 'red',
  },
  headerText: {
    fontWeight: 'bold',
    fontSize: 20,
  },
});
