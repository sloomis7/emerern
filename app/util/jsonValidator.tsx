export const validateStringField = (field: string | undefined) => {
  if (typeof field !== 'string') {
    return false;
  }
  if (field == undefined || field.length == 0) {
    return false;
  }
  return true;
};

export const validateStringFieldEmpty = (field: string | undefined) => {
  if (typeof field !== 'string') {
    return false;
  }
  if (field == undefined) {
    return false;
  }
  return true;
};

export const validateBoolField = (field: boolean | undefined) => {
  if (typeof field !== 'boolean') {
    return false;
  }

  if (field == undefined) {
    return false;
  }
  return true;
};

export const validateNumberField = (field: number | undefined) => {
  if (typeof field !== 'number') {
    return false;
  }

  if (field == undefined) {
    return false;
  }
  return true;
};

export const validateArrayField = (field: any | undefined) => {
  if (!Array.isArray(field)) {
    return false;
  }

  if (field == undefined) {
    return false;
  }
  return true;
};
