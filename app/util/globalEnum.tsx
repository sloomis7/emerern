export enum LoadingState {
  loading,
  loaded,
  error,
}

export enum Environment {
  dev,
  prod,
}

export enum Error {
  authFailed,
  networkError
}