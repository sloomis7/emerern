import AsyncStorage from '@react-native-community/async-storage';

export const storeData = async (key: string, value: any) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (e) {
    //console.log(e);
    // saving error
  }
};

export const removeData = async (key: string) => {
  try {
    await AsyncStorage.removeItem(key);
  }
  catch(e) {
    //console.log(e);
  }
}

export const getData = async (key: string, completion: (value: any) => void) => {
  try {
    const value = await AsyncStorage.getItem(key);
      completion(value)
  } catch (e) {
    // error reading value
    //console.log(e)
    //console.log('error1');
  }
};


export default {
  token: 'token',
};
