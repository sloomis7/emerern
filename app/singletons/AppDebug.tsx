import AppData from './AppData';
import {Environment} from '../util/globalEnum';

export default class AppDebug {
  appData = AppData.getInstance();

  static instance: AppDebug;

  signUpAutoFill = true;
  signInAutoFill = true;

  static getInstance() {
    if (AppDebug.instance == null) {
      AppDebug.instance = new AppDebug();
    }

    return AppDebug.instance;
  }

  getAutoTest1() {
    if (this.appData.env == Environment.dev) {
      return true;
    }
    return false;
  }
  getSignUpAutoFill() {
    return this.signUpAutoFill;
  }
  getSignInAutoFill() {
    return this.signInAutoFill;
  }
}
