import RequestManager from './RequestManager';
import endpoints from '../config/endpoints';
import AppData from './AppData';
import {Environment} from '../util/globalEnum';
import {Platform} from 'react-native';
import {validateBoolField} from '../util/jsonValidator';
import {removeData} from '../util/storage';
import storageKeys from '../util/storage';
import {Error} from '../util/globalEnum';

export default class WsManager {
  static instance: WsManager;

  requestManager = RequestManager.getInstance();
  appData = AppData.getInstance();

  port = '9000';
  localhost = Platform.OS === 'ios' ? '10.0.0.196' : '10.0.0.196';

  hosts = {
    prodHost: 'https://emere-go-backend.herokuapp.com/',
    devHost: 'http://' + this.localhost + ':' + this.port + '/',
  };

  authHeader = this.requestManager.getHeaders;

  static getInstance() {
    if (!WsManager.instance) {
      WsManager.instance = new WsManager();
    }

    return WsManager.instance;
  }

  getHost() {
    if (this.appData.env == Environment.dev) {
      return this.hosts.devHost;
    } else {
      return this.hosts.prodHost;
    }
  }

  isAuthorized(json: any) {
    if (validateBoolField(json.isAuthorized)) {
      let isAuthorized: boolean = json.isAuthorized;

      if (!isAuthorized) {
        //TODO: log out push
        removeData(storageKeys.token);
        return false;
      }

      return true;
    } else {
      return false;
    }
  }

  loginUser = (token: string, completion: (arg0: any, arg1?: Error) => void) => {
    this.authHeader.Token = token;

    this.requestManager.get(
      this.getHost() + endpoints.login,
      this.authHeader,
      (json: any, error?: Error) => {
        if (!this.isAuthorized(json)) {
          completion(null, Error.authFailed);
          return;
        }
        completion(json, error);
      },
    );
  };

  signUpUser = (state: any, completion: (arg0: any, arg1?: Error) => void) => {
    this.requestManager.post(
      this.getHost() + endpoints.signUp,
      state,
      undefined,
      (json: any, error?: Error) => {
        completion(json, error);
      },
    );
  };

  signInUser = (state: any, completion: (arg0: any, arg1?: Error) => void) => {
    this.requestManager.post(
      this.getHost() + endpoints.signIn,
      state,
      undefined,
      (json: any, error?: Error) => {
        completion(json, error);
      },
    );
  };

  getCategories = (completion: (arg0: any, arg1?: Error) => void) => {
    this.requestManager.get(
      this.getHost() + endpoints.getCategories,
      this.authHeader,
      (json: any, error?: Error) => {
        if (!this.isAuthorized(json)) {
          completion(null, Error.authFailed);
          return;
        }
        completion(json, error);
      },
    );
  };

  

  getProductDetails = (productID: string, completion: (arg0: any, arg1?: Error) => void) => {
    this.requestManager.get(
      this.getHost() + endpoints.getProductDetails + '/' + productID,
      this.authHeader,
      (json: any, error?: Error) => {
        if (!this.isAuthorized(json)) {
          completion(null, Error.authFailed);
          return;
        }
        completion(json, error);
      },
    );
  };
}
