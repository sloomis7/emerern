import {Environment} from '../util/globalEnum';

export default class AppData {
  static instance: AppData;
  env = Environment.prod;

  static getInstance() {
    if (AppData.instance == null) {
      AppData.instance = new AppData();
    }

    return AppData.instance;
  }
}
