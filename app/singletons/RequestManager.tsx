import {Error} from '../util/globalEnum';

export default class RequestManager {
  static instance: RequestManager;

  static getInstance() {
    if (RequestManager.instance == null) {
      RequestManager.instance = new RequestManager();
    }

    return RequestManager.instance;
  }

  getHeaders: any = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };

  postHeaders: any = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };

  get = (url: string, headers = this.getHeaders, completion: (arg0?: any, arg1?: Error) => void) => {
    
    //console.log("test")
    return fetch(url, {
      method: 'GET',
      headers: headers,
    })
      .then((response) => response.json())
      .then((json) => {
        completion(json);
      })
      .catch((error) => {
        //TODO: log if dev only
        console.error(error);
        completion(null, Error.networkError);
      });
  };

  post = (url: string, params: any, headers = this.postHeaders, completion: (arg0?: any, arg1?: Error) => void) => {
    return fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    })
      .then((response) => response.json())
      .then((json) => {
        completion(json);
      })
      .catch((error) => {
        //TODO: log if dev only
        console.error(error);
        completion(null, Error.networkError);
      });
  };
}
