import Realm, {User} from 'realm';
//import User from '../models/User'

export const USER_SCHEMA = 'User';

export const UserSchema = {
  name: USER_SCHEMA,
  primaryKey: 'id',
  properties: {
    id: 'string', //primary key
    fullName: {type: 'string', indexed: true},
    phoneNumber: {type: 'string', indexed: true},
    email: {type: 'string', indexed: true},
  },
};
const databaseOptions = {
  path: 'Emere.realm',
  schema: [UserSchema],
  schemaVersion: 0, //optional
};
//functions for User
export const insertNewUser = (newUser: User) =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        //('create db:', realm.path);
        realm.write(() => {
          realm.create(USER_SCHEMA, newUser);
          resolve(newUser);
        });
      })
      .catch((error) => {
        //console.log(error);
        reject(error);
      });
  });

// export const updateUser = (user: User) =>
//   new Promise<void>((resolve, reject) => {
//     Realm.open(databaseOptions)
//       .then((realm) => {
//         realm.write(() => {
//           let updatedUser: any = realm.objectForPrimaryKey(USER_SCHEMA, user.id);
//           updatedUser.fullName = user.fullName;

//           //realm.create(USER_SCHEMA, newUser);
//           resolve();
//         });
//       })
//       .catch((error) => reject(error));
//   });

export const deleteUser = (user: User) =>
  new Promise<void>((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          let deletedUser = realm.objectForPrimaryKey(USER_SCHEMA, user.id);

          realm.delete(deletedUser);
          resolve();
        });
      })
      .catch((error) => reject(error));
  });

export const getAllUsers = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        let allUsers = realm.objects(USER_SCHEMA);
        resolve(allUsers);
      })
      .catch((error) => reject(error));
  });

export const getUser = (userID: number) =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        let user = realm.objectForPrimaryKey(USER_SCHEMA, userID);
        if (user != undefined) {
          resolve(user);
        } else {
          reject(Error);
        }
      })
      .catch((error) => {
        //(error);
        reject(error);
      });
  });

export const deleteAllUsers = () =>
  new Promise<void>((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          let allUsers = realm.objects(USER_SCHEMA);
          realm.delete(allUsers);
          resolve();
        });
      })
      .catch((error) => reject(error));
  });

export default new Realm(databaseOptions);
