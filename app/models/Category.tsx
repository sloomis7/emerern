import {
  validateStringField,
  validateStringFieldEmpty,
  validateBoolField,
  validateArrayField,
} from '../util/jsonValidator';

export default class Category {
  id: string;
  name: string;
  products: Product[];
  isFeatured: boolean;
  imageUrl: string;

  constructor(json: any, products: Product[]) {
    this.id = json.id;
    this.name = json.name;
    this.products = products;
    this.isFeatured = json.isFeatured;
    this.imageUrl = json.imageUrl;
  }

  static createPlaceholderCategory(id_val: string){

    return new Category({id:id_val, name: "", isFeatured: false, imageUrl: ""}, [
      Product.createPlaceholderProduct('0'),
      Product.createPlaceholderProduct('1'),
      Product.createPlaceholderProduct('2'),
      Product.createPlaceholderProduct('3'),
      Product.createPlaceholderProduct('4'),
      Product.createPlaceholderProduct('5'),
    ])
  }

  static createCategory(json: any) {
    let finalProducts: Product[] = [];

    if (!validateArrayField(json.products)) {
      return;
    } else {
      let products = json.products;
      for (let product of products) {
        let newProduct = Product.createProduct(product);

        if (newProduct != null) {
          finalProducts.push(newProduct);
        }
      }
    }
    if (
      validateStringField(json.id) &&
      validateStringField(json.name) &&
      validateBoolField(json.isFeatured) &&
      Category.validateImageUrl(json.isFeatured, json.imageUrl)
      && finalProducts.length > 0
    ) {
      let category = new Category(json, finalProducts);

      return category;
    }

    return;
  }

  static createCategories(json: any): Category[] | undefined {
    if (validateArrayField(json)) {
      let categories: Category[] = [];

      for (let category of json) {
        let newCategory = this.createCategory(category);
        if (newCategory != null) {
          categories.push(newCategory);
        }
      }

      if (categories.length > 0) {
        return categories;
      }
    }

    return;
  }

  static validateImageUrl(isFeatured: boolean, imageUrl: any) {
    if (isFeatured) {
      if (validateStringFieldEmpty(imageUrl) == false) {
        return false;
      }
    } else {
      if (validateStringField(imageUrl) == false) {
        return false;
      }
    }
    return true;
  }
}

export class Product {
  id: string;
  name: string;
  price: string;
  imageUrl: string;

  constructor(json: any) {
    this.id = json.id;
    this.name = json.name;
    this.price = json.price;
    this.imageUrl = json.imageUrl;
  }

  static createPlaceholderProduct(id_val: string){

    return new Product({id:id_val, name: "", price: "", imageUrl: ""})
  }

  static createProduct(json: any) {
    if (
      validateStringField(json.id) &&
      validateStringField(json.name) &&
      validateStringField(json.price) &&
      validateStringField(json.imageUrl)
    ) {
      let product = new Product(json);

      return product;
    }

    return;
  }
}
