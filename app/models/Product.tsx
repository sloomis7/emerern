import {
  validateStringField,
  validateNumberField,
  validateArrayField,
  validateStringFieldEmpty,
} from '../util/jsonValidator';

export type ImageURL = {
  url: string;
  id: string;
};

export default class Product {
  id: string;
  name: string;
  brand: string;
  price: string;
  priceValue: number;
  oldPrice: string;
  imageUrls: ImageURL[];
  description: string;

  constructor(json: any, imageUrls: ImageURL[]) {
    this.id = json.id;
    this.name = json.name;
    this.brand = json.brand;
    this.price = json.price;
    this.priceValue = json.priceValue;
    this.oldPrice = json.oldPrice;
    this.description = json.description;
    this.imageUrls = imageUrls;
  }

  static createProduct(json: any) {
    let finalImageUrls: ImageURL[] = [];

    if (!validateArrayField(json.imageUrls)) {
      return;
    } else {
      let imageUrls = json.imageUrls;
      var count = 0;
      for (let imageUrl of imageUrls) {
        if (validateStringField(imageUrl) && finalImageUrls.length < 7) {
          let newImageURL = {url: imageUrl, id: count.toString()} as ImageURL;
          finalImageUrls.push(newImageURL);
          count++;
        }
      }
    }
    if (
      validateStringField(json.id) &&
      validateStringField(json.name) &&
      validateStringField(json.brand) &&
      validateStringField(json.price) &&
      validateNumberField(json.priceValue) &&
      validateStringFieldEmpty(json.oldPrice) &&
      validateStringField(json.description) &&
      finalImageUrls.length > 0
    ) {
      let product = new Product(json, finalImageUrls);

      return product;
    }

    return;
  }
}
