import {validateStringField} from '../util/jsonValidator';

export default class User {
  id: string;
  fullName: string;
  phoneNumber: string;
  email: string;
  //schemaDict;

  constructor(userJson: any) {
    this.id = userJson.id;
    this.fullName = userJson.fullName;
    this.phoneNumber = userJson.phoneNumber;
    this.email = userJson.email;
  }
  static createUser(userJson: any) {
    if (
      validateStringField(userJson.id) &&
      validateStringField(userJson.fullName) &&
      validateStringField(userJson.phoneNumber) &&
      validateStringField(userJson.email)
    ) {
      let user = new User(userJson);


      // user.schemaDict = {
      //   id: user.id,
      //   name: user.fullName,
      //   phoneNumber: user.phoneNumber,
      //   email: user.email,
      // };

      return user;
    }

    return;
  }
}
