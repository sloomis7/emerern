import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import colors from '../config/colors';
import fonts from '../config/fonts';
import LoginDC from '../dataControllers/LoginDC';
import {LoadingState} from '../util/globalEnum';

export default class LoginScreen extends Component {
  loginDC = new LoginDC(this.props);

  state = {
    loginWSState: LoadingState.loading,
  };
  constructor(props: any) {
    super(props);

    this.signUpAction = this.signUpAction.bind(this);
    this.signInAction = this.signInAction.bind(this);
  }
  componentDidMount() {
    this.loginDC.login((didSucceed) => {
      if (didSucceed) {
        //Push Home
        this.loginDC.navigateToHome(this.props);
      } else {
        if (this.loginDC.isAutoTest()) {
          this.signInAction();
        }
      }

      this.setState({loginWSState: LoadingState.loaded});
    });
  }

  signUpAction() {
    this.loginDC.navigateToSignUp(this.props);
  }

  signInAction() {
    this.loginDC.navigateToSignIn(this.props);
  }

  render() {
    return (
      <ImageBackground
        style={styles.background}
        source={require('../assets/background.jpg')}>
        <SafeAreaView style={styles.background}>
          <View style={styles.topHalf}>
            <View style={styles.logoContainer}>
              <Image
                style={styles.logo}
                source={require('../assets/logo.png')}
              />
            </View>
          </View>
          {this.state.loginWSState == LoadingState.loading ? (
            <View style={styles.bottomHalf}></View>
          ) : (
            <View style={styles.bottomHalf}>
              <View style={styles.buttonView}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={this.signInAction}>
                  <Text style={styles.buttonText}>Sign In</Text>
                </TouchableOpacity>
              </View>

              <View style={styles.buttonView}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={this.signUpAction}>
                  <Text style={styles.buttonText}>Create Account</Text>
                </TouchableOpacity>
              </View>
              <View style={{height: '3%'}} />
            </View>
          )}
        </SafeAreaView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  topHalf: {
    flex: 1,
    width: '100%',
  },
  bottomHalf: {
    flex: 1,
    width: '100%',
    justifyContent: 'flex-end',
  },
  background: {
    height: '100%',
    width: '100%',
    alignItems: 'center',
    resizeMode: 'cover',
  },
  logo: {
    width: 200,
    height: 200,
  },
  logoContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    width: '75%',
    height: '80%',
    alignItems: 'center',
    backgroundColor: colors.primary,
    justifyContent: 'center',
    borderRadius: 10,
    //borderColor: colors.black,
    //borderWidth: 1,
  },
  buttonView: {
    width: '100%',
    height: '15%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: colors.white,
    fontFamily: fonts.primary_bold,
    fontSize: 16,
  },
});
