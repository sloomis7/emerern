import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  Animated,
  TouchableWithoutFeedback,
  Easing,
} from 'react-native';
export default class FavoritesScreen extends Component {
  isExpanded = false;
  spinValue = new Animated.Value(0);

  render() {
    const expandDetails = (val: number) => {
      //Animated.loop(
      Animated.timing(this.spinValue, {
        toValue: val,
        duration: 200,
        easing: Easing.linear, // Easing is an additional import from react-native
        useNativeDriver: true, // To make use of native driver for performance
      }).start(() => {
        this.isExpanded = !this.isExpanded;
      });
    };

    // Next, interpolate beginning and end values (in this case 0 and 1)
    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['-90deg', '90deg'],
    });

    const animatedStyle = {
      transform: [{rotate: spin}],
    };

    return (
      <SafeAreaView style={styles.background}>
        <View style={styles.expandContainer}>
          <TouchableWithoutFeedback
            onPress={() => expandDetails(this.isExpanded ? 0 : 1)}>
            <Animated.Image
              style={[styles.expandImage, animatedStyle]}
              source={require('../assets/left_chevron.png')}
            />
          </TouchableWithoutFeedback>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
  },
  expandContainer: {
    flex: 1,
    backgroundColor: 'green',
    alignItems: 'center',
    justifyContent: 'center',
  },
  expandImage: {
    height: 30,
    width: 30,
    //transform: [{rotate: '270deg'}],
  },
});
