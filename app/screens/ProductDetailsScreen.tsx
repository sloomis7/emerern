import React, {Component, createRef} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Alert,
  View,
  Dimensions,
  Text,
  Image,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Animated,
  LayoutAnimation,
  Easing,
  UIManager,
  Platform,
} from 'react-native';
import ProductDetailsDC from '../dataControllers/ProductDetailsDC';
import {LoadingState} from '../util/globalEnum';
import ImageScrollView from '../ui/views/ProductDetailsScreen/ImageScrollView';
import LeftButton from '../ui/navigation_bar/leftButton';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import colors from '../config/colors';
import fonts from '../config/fonts';
import LinearGradient from 'react-native-linear-gradient';
import {ImageURL} from '../models/Product';
import {ScrollView} from 'react-native-gesture-handler';
import resolveAssetSource from 'resolveAssetSource';

export default class ProductDetailsScreen extends Component {
  constructor(props: any) {
    super(props);
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental &&
        UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }
  productDetailsDC: ProductDetailsDC = new ProductDetailsDC(this.props);

  state = {
    getProductLoadingState: LoadingState.loading,
    currentScrollIndex: 0,
    descriptionHeight: descriptionStartingHeight,
    detailsExpanded: false,
    isFavorite: false,
  };

  dataArray: ImageURL[] | undefined;

  scrollViewRef: any = createRef();
  gradientViewRef: any = createRef();
  favoriteImageRef: any = createRef();

  spinValue = new Animated.Value(0);
  isExpanded = false;

  //TODO: read from user favorites

  componentDidMount() {
    let {navigation}: any = this.props;

    this.productDetailsDC.loadDetails((didSucceed, authFailed) => {
      if (authFailed) {
        navigation.setOptions({
          animationEnabled: false,
        });
        navigation.popToTop();
        return;
      }
      //this.productDetailsDC.product!.description = 'test \n test \n test';

      if (!didSucceed) {
        navigation.pop();
        Alert.alert(
          'Error',
          'Failed to load Product Details',
          [{text: 'OK', onPress: () => {}}],
          {cancelable: false},
        );
      } else {
        let dataSource = [...this.productDetailsDC.product!.imageUrls];
        var circularArray = dataSource;
        if (circularArray.length > 1) {
          circularArray.splice(0, 0, {...dataSource[dataSource.length - 1]});
          circularArray[0].id = '-1';
          circularArray.push({...dataSource[1]});
          circularArray[circularArray.length - 1].id = (
            circularArray.length - 1
          ).toString();
        }
        this.dataArray = circularArray;

        this.setState({getProductLoadingState: LoadingState.loaded});
      }
    });
  }

  didScroll = (index: number) => {
    //console.log('did scroll');
    this.setState({currentScrollIndex: index});
  };

  render() {
    const loading =
      this.state.getProductLoadingState == LoadingState.loading ? true : false;
    const {navigation}: any = this.props;
    const windowHeight = Dimensions.get('window').height;

    const addToCartAction = () => {};

    const checkoutAction = () => {};

    const favoriteAction = () => {
      console.log('favorite Action');
      // if (this.isFavorite) {
      //   this.favoriteImageRef.current.setNativeProps({
      //     source: require('../assets/tabBarIcons/favoriteOff.png'),
      //   });
      // } else {
      //   this.favoriteImageRef.current.setNativeProps({
      //     source: [
      //       resolveAssetSource(require('../assets/tabBarIcons/favorite.png')),
      //     ],
      //   });
      // }

      // callback spinner
      this.setState({isFavorite: !this.state.isFavorite});
    };

    const expandDetails = () => {
      LayoutAnimation.configureNext(
        LayoutAnimation.create(
          500,
          LayoutAnimation.Types.easeInEaseOut,
          LayoutAnimation.Properties.scaleXY,
        ),
      );

      Animated.timing(this.spinValue, {
        toValue: this.isExpanded ? 0 : 1,
        duration: 200,
        easing: Easing.linear, // Easing is an additional import from react-native
        useNativeDriver: true, // To make use of native driver for performance
      }).start(() => {
        this.isExpanded = !this.isExpanded;

        if (!this.isExpanded) {
          this.scrollViewRef.current.scrollTo({x: 0, y: -44, animated: true});
        }
        this.setState({
          detailsExpanded: !this.state.detailsExpanded,
        });
      });
    };

    // Next, interpolate beginning and end values (in this case 0 and 1)
    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['-90deg', '90deg'],
    });

    const animatedStyle = {
      transform: [{rotate: spin}],
    };

    const shouldShowGradient = () => {
      if (
        !loading &&
        this.state.descriptionHeight > descriptionStartingHeight
      ) {
        return true;
      }

      return false;
    };

    return (
      <View style={styles.background}>
        <ScrollView
          contentContainerStyle={styles.scrollView}
          contentInset={{top: 44, left: 0, bottom: 34, right: 0}}
          contentOffset={{x: 0, y: -44}}
          ref={this.scrollViewRef}
          scrollEnabled={false}
          scrollIndicatorInsets={{top: 0, left: 0, bottom: 0, right: 0}}
          onContentSizeChange={(contentWidth, contentHeight) => {
            if (loading) {
              this.scrollViewRef.current.setNativeProps({scrollEnabled: true});
              return;
            }
            let top = 44;
            let bottom = 0;
            let total = top + bottom;
            if (contentHeight > windowHeight - total) {
              this.scrollViewRef.current.setNativeProps({scrollEnabled: true});
            } else {
              this.scrollViewRef.current.setNativeProps({scrollEnabled: false});
            }
          }}>
          <View style={styles.imageView}>
            {loading ? (
              <SkeletonPlaceholder>
                <View style={skeletonStyles.image}></View>
              </SkeletonPlaceholder>
            ) : (
              <>
                <ImageScrollView
                  dataSource={this.dataArray!}
                  didScrollHandler={this.didScroll}
                />
                <View style={styles.dotContainer} pointerEvents="box-none">
                  <TouchableWithoutFeedback onPress={favoriteAction}>
                    <View style={styles.favoritesView}>
                      <Image
                        ref={this.favoriteImageRef}
                        style={styles.favoriteImage}
                        source={
                          this.state.isFavorite
                            ? require('../assets/tabBarIcons/favorite.png')
                            : require('../assets/tabBarIcons/favoriteOff.png')
                        }></Image>
                    </View>
                  </TouchableWithoutFeedback>

                  <DotView
                    items={this.productDetailsDC.product!.imageUrls}
                    state={this.state}></DotView>
                </View>
              </>
            )}
            <View style={styles.backHeader}>
              <LeftButton navigation={navigation} isDown={true} />
            </View>
          </View>
          <View style={styles.detailsView}>
            {loading ? (
              <View>
                <SkeletonPlaceholder>
                  <View style={skeletonStyles.detailsText1}></View>
                  <View style={skeletonStyles.detailsText2}></View>
                  <View style={skeletonStyles.line}></View>
                  <View style={skeletonStyles.detailsText3}></View>
                  <View style={skeletonStyles.detailsText4}></View>
                  <View style={skeletonStyles.detailsText3}></View>
                  <View style={skeletonStyles.detailsText4}></View>
                </SkeletonPlaceholder>
                {/* <Text style={styles.detailsText1}>Test1</Text>
              <Text style={styles.detailsText2}>Test2</Text>
              <View style={styles.line}></View>
              <Text style={styles.detailsText3}>Test3</Text> */}
              </View>
            ) : (
              <>
                <Text style={styles.detailsText1}>
                  {this.productDetailsDC.product!.name}
                </Text>
                <View style={styles.priceContainer}>
                  <Text style={styles.detailsText2}>
                    {this.productDetailsDC.product!.price}
                  </Text>
                  {this.productDetailsDC.product!.oldPrice != '' ? (
                    <Text style={styles.oldPriceText}>
                      {this.productDetailsDC.product!.oldPrice}
                    </Text>
                  ) : (
                    <></>
                  )}
                </View>

                <View style={styles.line}></View>
                <View
                  style={[
                    styles.detailsContainer,
                    this.state.detailsExpanded
                      ? {height: this.state.descriptionHeight + margin * 2}
                      : descriptionStartingHeight < this.state.descriptionHeight
                      ? {height: descriptionStartingHeight}
                      : {height: this.state.descriptionHeight + margin * 2},
                  ]}>
                  <View
                    style={{
                      flex: 1,
                      justifyContent: 'flex-start',
                    }}>
                    <Text
                      style={styles.detailsText3}
                      onLayout={(event) => {
                        var {x, y, width, height} = event.nativeEvent.layout;
                        this.setState({descriptionHeight: height});
                      }}>
                      {this.productDetailsDC.product!.description}
                    </Text>
                  </View>
                  {shouldShowGradient() ? (
                    <LinearGradient
                      ref={this.gradientViewRef}
                      colors={[
                        'rgba(255, 255, 255, 0)',
                        'rgba(255, 255, 255, 1)',
                      ]}
                      style={
                        !this.state.detailsExpanded
                          ? styles.gradientView
                          : [styles.gradientView, {height: 0}]
                      }></LinearGradient>
                  ) : (
                    <></>
                  )}
                </View>
              </>
            )}

            {!loading &&
            this.state.descriptionHeight >= descriptionStartingHeight ? (
              <TouchableWithoutFeedback onPress={expandDetails}>
                <View style={styles.expandContainer}>
                  <Animated.Image
                    style={[styles.expandImage, animatedStyle]}
                    source={require('../assets/left_chevron.png')}
                  />
                </View>
              </TouchableWithoutFeedback>
            ) : (
              <></>
            )}
          </View>

          {!loading ? (
            <View style={styles.buttonContainer}>
              <TouchableOpacity style={{flex: 1}} onPress={addToCartAction}>
                <View style={styles.addToCartButton}>
                  <Text style={styles.addToCartText}>Add To Cart</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{flex: 1}} onPress={checkoutAction}>
                <View style={styles.checkOutButton}>
                  <Text style={styles.checkoutText}>Buy</Text>
                </View>
              </TouchableOpacity>
            </View>
          ) : (
            <>
              <View
                style={{
                  width: '100%',
                  height: 40,
                  //backgroundColor: 'blue',
                }}></View>
              <SkeletonPlaceholder>
                <View style={skeletonStyles.buttonContainer}>
                  <View style={skeletonStyles.addToCartButton}></View>
                  <View style={skeletonStyles.checkOutButton}></View>
                </View>
              </SkeletonPlaceholder>
              {/* <View style={skeletonStyles.buttonContainer}></View> */}
            </>
          )}
        </ScrollView>
      </View>
    );
  }
}

interface Props {
  items: any[];
  state: any;
}

const DotView = ({items, state}: Props) => {
  if (items.length < 2) {
    return <></>;
  }

  return (
    <View style={styles.dotView} pointerEvents="none">
      {items.map((object, i) => {
        if (i == state.currentScrollIndex) {
          return (
            <View
              style={[styles.dot, {backgroundColor: colors.placeholderGray2}]}
              key={i}></View>
          );
        }
        return <View style={styles.dot} key={i}></View>;
      })}
    </View>
  );
};

const windowWidth = Dimensions.get('window').width;
const dotSize = 10;
const descriptionStartingHeight = 100;
const margin = 10;
const buttonHeight = 80;

const styles = StyleSheet.create({
  background: {
    flex: 1,
    //backgroundColor: 'pink',
    backgroundColor: colors.white,
    alignItems: 'center',
  },
  scrollView: {
    //backgroundColor: 'orange',
    backgroundColor: colors.white,
    alignItems: 'center',
  },
  backHeader: {
    position: 'absolute',
    //backgroundColor: 'green',
    width: '100%',
    marginVertical: margin * 2,
    marginLeft: margin,
  },
  imageView: {
    width: windowWidth,
    height: windowWidth,
    alignItems: 'flex-start',
  },
  image: {
    width: windowWidth,
    height: windowWidth,
  },
  dotContainer: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    //backgroundColor: 'blue',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  dotView: {
    height: dotSize,
    //backgroundColor: 'pink',
    marginBottom: margin * 3,
    flexDirection: 'row',
    paddingHorizontal: 4,
  },
  dot: {
    width: dotSize,
    height: dotSize,
    borderRadius: 30,
    backgroundColor: colors.lineGray5,
    marginHorizontal: 4,
  },
  favoritesView: {
    position: 'absolute',
    alignSelf: 'flex-start',
    backgroundColor: colors.white,
    width: 40,
    height: 40,
    bottom: 20,
    left: 20,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  favoriteImage: {
    width: 30,
    height: 30,
    borderRadius: 30,
  },
  detailsView: {
    width: '100%',
    //backgroundColor: 'teal',
    paddingTop: margin,
    paddingHorizontal: margin,
  },
  detailsText1: {
    fontFamily: fonts.primary_bold,
    fontSize: 18,
    marginVertical: margin,
    marginLeft: margin,
  },
  detailsText2: {
    fontFamily: fonts.primary,
    color: colors.lineGray5,
    fontSize: 18,
    marginVertical: margin,
    marginLeft: margin,
  },
  detailsContainer: {
    width: '100%',
    //height: descriptionStartingHeight,
    //backgroundColor: 'red',
    overflow: 'hidden',
    justifyContent: 'flex-end',
  },
  detailsText3: {
    position: 'absolute',
    fontFamily: fonts.primary,
    fontSize: 16,
    margin: margin,
    lineHeight: 22,
  },
  line: {
    height: 1,
    marginVertical: margin,
    backgroundColor: colors.lineGray5,
  },
  priceContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  oldPriceText: {
    fontFamily: fonts.primary,
    color: colors.primary,
    fontSize: 16,
    marginVertical: margin,
    marginLeft: margin,
    textDecorationLine: 'line-through',
  },
  expandContainer: {
    width: '40%',
    height: 40,
    //backgroundColor: 'green',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  expandImage: {
    height: 30,
    width: 30,
  },
  gradientView: {
    position: 'absolute',
    height: 60,
    width: '100%',
  },
  buttonContainer: {
    width: '100%',
    backgroundColor: colors.white,
    flexDirection: 'row',
    padding: margin,
  },
  addToCartButton: {
    //flex: 1,
    height: buttonHeight,
    backgroundColor: colors.black,
    marginRight: margin / 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  checkOutButton: {
    //flex: 1,
    height: buttonHeight,
    backgroundColor: colors.white,
    marginLeft: margin / 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    borderColor: colors.black,
    borderWidth: 2,
  },
  addToCartText: {
    fontFamily: fonts.primary_bold,
    color: colors.white,
    fontSize: 16,
  },
  checkoutText: {
    fontFamily: fonts.primary_bold,
    color: colors.black,
    fontSize: 16,
  },
});

const skeletonStyles = StyleSheet.create({
  imageView: {
    width: windowWidth,
    height: windowWidth,
  },
  image: {
    width: windowWidth,
    height: windowWidth,
  },
  detailsText1: {
    //position: 'absolute',
    marginLeft: 10,
    height: 18,
    width: 240,
    marginVertical: 10,
  },
  detailsText2: {
    //position: 'absolute',
    marginLeft: 10,
    height: 18,
    width: 80,
    marginVertical: 10,
  },
  detailsText3: {
    //position: 'absolute',
    marginLeft: 10,
    height: 16,
    width: '90%',
    marginVertical: 3,
  },
  detailsText4: {
    //position: 'absolute',
    marginLeft: 10,
    height: 16,
    width: '80%',
    marginVertical: 3,
  },
  line: {
    ///width: '100%',
    //marginHorizontal: 10,
    //height: 1,
    marginTop: 10.5,
    marginBottom: 20.5,
  },
  buttonContainer: {
    width: '100%',
    //backgroundColor: colors.white,
    flexDirection: 'row',
    padding: margin,
  },
  addToCartButton: {
    //flex: 1,
    height: buttonHeight,
    //backgroundColor: colors.black,
    marginRight: margin / 2,
    borderRadius: 5,
    width: windowWidth / 2 - margin * 1.5,
  },
  checkOutButton: {
    //flex: 1,
    height: buttonHeight,
    marginLeft: margin / 2,
    borderRadius: 5,
    width: windowWidth / 2 - margin * 1.5,
  },
});
