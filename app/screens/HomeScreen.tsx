import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Platform,
  Animated,
} from 'react-native';
import {FlatList, ScrollView} from 'react-native-gesture-handler';
import {LoadingState} from '../util/globalEnum';
import colors from '../config/colors';
import fonts from '../config/fonts';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import AnimatedImage from '../ui/image/AnimatedImage';
import NewArrivalView from '../ui/animatedViews/HomeScreen/NewArrivalView';
import HomeDC from '../dataControllers/HomeDC';
import Category, {Product} from '../models/Category';
import FeaturedCategoryView from '../ui/views/HomeScreen/FeaturedCategoryView';
import HomeCategoryView from '../ui/views/HomeScreen/HomeCategoryView';

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

export default class HomeScreen extends Component {
  homeDC: HomeDC = new HomeDC(this.props);

  state = {
    getCategoriesWSState: LoadingState.loading,
  };

  componentDidMount() {
    this.homeDC.loadCategories((didSucceed, authFailed) => {
      if (authFailed) {
        let {navigation}: any = this.props;
        navigation.setOptions({
          animationEnabled: false,
        });
        navigation.popToTop();
        return;
      }
      this.setState({getCategoriesWSState: LoadingState.loaded});
    });
  }
  render() {
    return (
      <>
        {this.state.getCategoriesWSState == LoadingState.loading
          ? this.mainSkeletonView()
          : this.mainView()}
      </>
    );
  }

  mainView() {
    return (
      <>
        <SafeAreaView style={styles.background}>
          <ScrollView style={styles.scrollView}>
            {this.state.getCategoriesWSState != LoadingState.loading &&
            this.homeDC.categories == null ? (
              <View></View>
            ) : (
              this.categoriesScrollContainer()
            )}
            {this.state.getCategoriesWSState != LoadingState.loading &&
            this.homeDC.newArrivalCategory == null ? (
              <View></View>
            ) : (
              this.newArrivalScrollContainer(this.homeDC.newArrivalCategory!)
            )}
            <FeaturedCategoryView
              category={this.homeDC.featuredCategory!}
              loading={false}
              selectionHandler={this.homeDC.didSelectProduct}
            />
            <HomeCategoryView
              category={this.homeDC.bestSellersCategory!}
              loading={false}
              selectionHandler={this.homeDC.didSelectProduct}
            />
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }

  mainSkeletonView() {
    return (
      <>
        <View style={{backgroundColor: 'white'}}>
          <View style={skeletonStyles.scrollView}>
            {this.categoriesSkeletonScrollContainer()}
            {this.newArrivalScrollContainer(this.homeDC.emptyCategory)}
            <FeaturedCategoryView
              category={this.homeDC.emptyCategory}
              loading={true}
            />
            <HomeCategoryView
              category={this.homeDC.emptyCategory}
              loading={true}
            />
          </View>
        </View>
      </>
    );
  }

  categoriesScrollContainer() {
    return (
      <>
        {this.homeDC.categories == null ? (
          <View></View>
        ) : (
          <View style={styles.categoriesScrollContainer}>
            <FlatList
              contentContainerStyle={styles.categoryFlatList}
              horizontal={true}
              keyExtractor={(item: Category) => item.id}
              data={this.homeDC.categories}
              showsHorizontalScrollIndicator={false}
              renderItem={({item}) => (
                <View style={styles.categoryView}>
                  <AnimatedImage
                    defaultImageSource={require('../assets/default.png')}
                    source={{
                      uri: item.imageUrl,
                    }}
                    style={styles.categoryImage}
                    ID={item.id}
                  />
                  <View style={styles.categoryImageOverlay}></View>

                  <Text style={styles.categoryViewText}>
                    {item.name.toUpperCase()}
                  </Text>
                </View>
              )}
            />
          </View>
        )}
      </>
    );
  }

  categoriesSkeletonScrollContainer() {
    return (
      <SkeletonPlaceholder>
        <View style={skeletonStyles.categoriesScrollContainer}>
          <View style={skeletonStyles.categoryView}></View>
          <View style={skeletonStyles.categoryView}></View>
          <View style={skeletonStyles.categoryView}></View>
        </View>
      </SkeletonPlaceholder>
    );
  }

  newArrivalScrollContainer(category: Category) {
    const x = new Animated.Value(
      windowWidth - cardWidth / 2 - cardMargin - cardMargin / 4,
    );

    const loading =
      this.state.getCategoriesWSState == LoadingState.loading ? false : true;

    const selectionHandler = this.homeDC.didSelectProduct;
    if (loading) {
    }
    //console.log('load test1');
    return (
      <View style={styles.newArrivalsScrollContainer}>
        {this.state.getCategoriesWSState == LoadingState.loading ? (
          <View style={styles.newArrivalHeader}>
            <View style={skeletonStyles.header1}>
              <SkeletonPlaceholder>
                <View style={skeletonStyles.header2}></View>
              </SkeletonPlaceholder>
            </View>

            <Text style={styles.newArrivalText}> </Text>
          </View>
        ) : (
          <View style={styles.newArrivalHeader}>
            <Text style={styles.newArrivalText}>
              {this.homeDC.newArrivalCategory?.name}
            </Text>
          </View>
        )}
        <AnimatedFlatList
          contentContainerStyle={styles.newArrivalFlatList}
          horizontal={true}
          keyExtractor={(item: any) => item.id}
          decelerationRate={0}
          snapToInterval={
            Platform.OS === 'android'
              ? cardWidth + cardMargin
              : cardWidth + cardMargin + 1
          } //your element width
          snapToAlignment={'center'}
          scrollEnabled={loading}
          pagingEnabled={true}
          contentOffset={{
            x:
              Platform.OS === 'android'
                ? cardWidth + cardMargin
                : windowWidth - cardWidth / 2 - cardMargin - cardMargin / 4,
            y: 0,
          }}
          contentInset={{
            // iOS ONLY
            top: 0,
            left: cardSide - cardMargin / 2, // Left spacing for the very first card
            bottom: 0,
            right: cardSide - cardMargin / 2, // Right spacing for the very last card
          }}
          data={category.products}
          showsHorizontalScrollIndicator={false}
          renderItem={({index, item}) =>
            this.state.getCategoriesWSState == LoadingState.loading ? (
              <NewArrivalView
                index={index}
                product={item as Product}
                x={x}
                loading={loading}
                selectionHandler={selectionHandler}
              />
            ) : (
              <NewArrivalView
                index={index}
                product={item as Product}
                x={x}
                loading={loading}
                selectionHandler={selectionHandler}
              />
            )
          }
          onScroll={Animated.event([{nativeEvent: {contentOffset: {x: x}}}], {
            useNativeDriver: true,
          })}
          scrollEventThrottle={16}
        />
      </View>
    );
  }
}
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const cardWidth = windowWidth * 0.7;
const sides = windowWidth - cardWidth;
const cardSide = sides / 2;
const cardMargin = 15;
const newArrivalsHeight = windowHeight * 0.5;

const styles = StyleSheet.create({
  background: {
    flex: 1,
    //backgroundColor: 'red',
    backgroundColor: colors.white,
    //alignItems: 'center',
    justifyContent: 'center',
  },
  categoriesScrollContainer: {
    height: windowWidth * 0.27,
    width: '100%',
    backgroundColor: colors.white,
  },
  scrollView: {
    width: '100%',
    //backgroundColor: 'green',
  },
  categoryView: {
    width: windowWidth * 0.27 * 1.4,
    height: windowWidth * 0.27 - 30,
    backgroundColor: colors.white,
    marginRight: 15,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  categoryFlatList: {
    alignItems: 'center',
    backgroundColor: colors.white,
    paddingLeft: 15,
  },
  categoryViewText: {
    textAlign: 'center',
    fontFamily: fonts.primary_bold,
    fontSize: 16,
    color: colors.white,
    padding: 8,
  },
  categoryImage: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
  },
  categoryImageOverlay: {
    position: 'absolute',
    opacity: 0.5,
    backgroundColor: colors.black,
    width: '100%',
    height: '100%',
  },
  newArrivalsScrollContainer: {
    width: '100%',
    //backgroundColor: 'yellow',
  },
  newArrivalFlatList: {
    alignItems: 'center',
    backgroundColor: colors.white,
    paddingHorizontal:
      Platform.OS === 'android' ? cardSide - cardMargin / 2 : 0,
    paddingVertical: 10,
  },
  newArrivalShadowView: {
    position: 'absolute',
    shadowOpacity: 0.8,
    shadowColor: colors.black,
    width: '100%',
    height: '100%',
  },
  newArrivalHeader: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-end',
    //backgroundColor: 'pink',
  },
  newArrivalText: {
    textAlign: 'center',
    fontFamily: fonts.primary,
    fontSize: 25,
    color: colors.wordGray5,
    marginBottom: 10,
    marginTop: 20,
    //backgroundColor: 'green',
  },
});

const skeletonStyles = StyleSheet.create({
  scrollView: {
    width: '100%',
  },
  categoriesScrollContainer: {
    height: windowWidth * 0.27,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 15,
  },
  categoryView: {
    width: windowWidth * 0.27 * 1.4,
    height: windowWidth * 0.27 - 30,
    marginRight: 15,
    borderRadius: 10,
  },
  newArrivalsScrollContainer: {
    height: newArrivalsHeight,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: -(cardWidth - cardSide + cardMargin) + 26,
  },
  featuredScrollContainer: {
    height: windowHeight * 0.5,
    width: '100%',
    //backgroundColor: 'teal',
    paddingVertical: 25,
  },
  header1: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'flex-end',
    //backgroundColor: 'pink',
  },
  header2: {
    width: 150,
    height: 26,
    marginBottom: 10,
  },
});
