import React, {Component} from 'react';
import {View, Text, Button, StyleSheet, TouchableOpacity, Alert} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import colors from '../config/colors';
import fonts from '../config/fonts';
import SignUpDC from '../dataControllers/SignUpDC';

//TODO: - make reusable textfield component - abstract
//Localization

export default class SignUpScreen extends Component {
  signUpDC: SignUpDC = new SignUpDC(this.props);

  state = {
    fullName: '',
    phoneNumber: '',
    email: '',
    password: '',
  };

  handleText = (text: string, fieldID: number) => {
    switch (fieldID) {
      case 1:
        this.setState({fullName: text});
        break;
      case 2:
        this.setState({phoneNumber: text});
        break;
      case 3:
        this.setState({email: text});
        break;
      case 4:
        this.setState({password: text});
        break;
      default:
        break;
    }
  };

  signUpAction = () => {
    //validate
    if (this.state.fullName == '') {
      //console.log('Full Name error');
    } else if (this.state.phoneNumber == '') {
      //console.log('Phone Number error');
    } else if (this.state.email == '') {
      //console.log('Email error');
    } else if (this.state.password == '') {
      //console.log('Password error');
    } else {

      this.signUpDC.signUp(this.state, (didSucceed: boolean, message: string) => {
        if (didSucceed) {
          //console.log('did succeed');

          //Push Home
          this.signUpDC.navigateToHome(this.props);

        } else {
          var errorMessage: string;
            if (message == '') {
              errorMessage = 'Something went wrong. Please Try again later.';
            } else {
              errorMessage = message;
            }

            Alert.alert(
              'Sign Up Error',
              errorMessage,
              [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
              ],
              {cancelable: false},
            );
        }
      });
    }
  };

  componentDidMount() {
    if (this.signUpDC.isAutoTest()) {
      this.setState({fullName: 'test1'});
      this.setState({phoneNumber: '12345'});
      this.setState({email: 'test@test.com'});
      this.setState({password: 'test123'});
    }
  }

  render() {
    return (
      <View style={styles.main}>
        <View style={styles.headerView}>
          <Text style={styles.headerText}>Create new account</Text>
        </View>
        <View style={styles.textFieldsView}>
          <View style={styles.textField}>
            <TextInput
              maxLength={40}
              style={styles.textInput}
              autoCapitalize={'words'}
              autoCompleteType={'off'}
              autoCorrect={false}
              clearButtonMode={'while-editing'}
              placeholder="Full Name"
              returnKeyType={'next'}
              onChangeText={(text) => this.handleText(text, 1)}
              value={this.state.fullName}
            />
          </View>
          <View style={styles.textField}>
            <TextInput
              maxLength={40}
              style={styles.textInput}
              autoCompleteType={'off'}
              autoCorrect={false}
              clearButtonMode={'while-editing'}
              placeholder="Phone Number"
              returnKeyType={'next'}
              onChangeText={(text) => this.handleText(text, 2)}
              keyboardType={'number-pad'}
              value={this.state.phoneNumber}
            />
          </View>
          <View style={styles.textField}>
            <TextInput
              maxLength={40}
              style={styles.textInput}
              autoCompleteType={'off'}
              autoCapitalize={'none'}
              autoCorrect={false}
              clearButtonMode={'while-editing'}
              placeholder="E-mail Address"
              returnKeyType={'next'}
              onChangeText={(text) => this.handleText(text, 3)}
              keyboardType={'email-address'}
              value={this.state.email}
            />
          </View>
          <View style={styles.textField}>
            <TextInput
              maxLength={40}
              style={styles.textInput}
              autoCompleteType={'off'}
              autoCapitalize={'none'}
              autoCorrect={false}
              clearButtonMode={'while-editing'}
              placeholder="Password"
              returnKeyType={'done'}
              onChangeText={(text) => this.handleText(text, 4)}
              value={this.state.password}
            />
          </View>
        </View>
        <View style={styles.buttonView}>
          <TouchableOpacity style={styles.button} onPress={this.signUpAction}>
            <Text style={styles.buttonText}>Sign Up</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colors.white,
  },
  headerView: {
    //backgroundColor: 'pink',
  },
  textFieldsView: {
    //backgroundColor: 'blue',
  },
  headerText: {
    color: colors.primary,
    fontFamily: fonts.primary_bold,
    fontSize: 35,
    //backgroundColor: 'teal',
    margin: '5%',
  },
  textField: {
    margin: '9%',
    marginVertical: '4%',
    height: 50,
    //backgroundColor: 'orange',
    borderWidth: 1,
    borderColor: colors.lineGray5,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    backgroundColor: colors.primary,
    marginVertical: '5%',
    width: '70%',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
  },
  buttonView: {
    //backgroundColor: 'yellow',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: colors.white,
    fontFamily: fonts.primary,
    fontSize: 20,
    marginVertical: '3%',
  },
  textInput: {
    width: '85%',
    height: '100%',
    fontFamily: fonts.primary,
    fontSize: 18,
  },
});
