import React, {Component} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    Text,
    View,
  } from 'react-native';
export default class TemplateScreen extends Component {
  render() {
    return (
      <SafeAreaView style={styles.background}>
        <View><Text>Test1</Text></View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center'
  },
});
