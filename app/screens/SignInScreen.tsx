import React, {Component} from 'react';
import {
  View,
  Text,
  Button,
  StyleSheet,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import colors from '../config/colors';
import fonts from '../config/fonts';
import SignInDC from '../dataControllers/SignInDC';

//TODO: - make reusable textfield component - abstract
//Login with Facebook
//Localization

export default class SignInScreen extends Component {
  signInDC: SignInDC = new SignInDC(this.props);

  state = {
    email: '',
    password: '',
  };

  handleText = (text: string, fieldID: number) => {
    switch (fieldID) {
      case 1:
        this.setState({email: text});
        break;
      case 2:
        this.setState({password: text});
        break;
      default:
        break;
    }
  };

  signInAction = () => {
    //validate

    if (this.state.email == '') {
      //console.log('Email error');
    } else if (this.state.password == '') {
      //console.log('Password error');
    } else {
      //console.log('test1');
      // send to backend
      this.signInDC.signIn(
        this.state,
        (didSucceed: boolean, message: string) => {
          if (didSucceed) {
            //console.log('did succeed');

            //Push Home
            this.signInDC.navigateToHome(this.props);
          } else {
            var errorMessage: string;
            if (message == '') {
              errorMessage = 'Something went wrong. Please Try again later.';
            } else {
              errorMessage = message;
            }

            Alert.alert(
              'Log in Error',
              errorMessage,
              [{text: 'OK', onPress: () => console.log('OK Pressed')}],
              {cancelable: false},
            );
          }
        },
      );
    }
  };

  componentDidMount() {
    if (this.signInDC.isAutoTest()) {
      this.setState({email: 'test@test.com'});
      this.setState({password: 'test123'});
    }
  }

  render() {
    return (
      <View style={styles.main}>
        <View style={styles.headerView}>
          <Text style={styles.headerText}>Sign In</Text>
        </View>
        <View style={styles.textFieldsView}>
          <View style={styles.textField}>
            <TextInput
              maxLength={40}
              style={styles.textInput}
              autoCompleteType={'off'}
              autoCapitalize={'none'}
              autoCorrect={false}
              clearButtonMode={'while-editing'}
              placeholder="E-mail Address"
              returnKeyType={'next'}
              onChangeText={(text) => this.handleText(text, 1)}
              keyboardType={'email-address'}
              value={this.state.email}
            />
          </View>
          <View style={styles.textField}>
            <TextInput
              maxLength={40}
              style={styles.textInput}
              autoCompleteType={'off'}
              autoCapitalize={'none'}
              autoCorrect={false}
              clearButtonMode={'while-editing'}
              placeholder="Password"
              returnKeyType={'done'}
              onChangeText={(text) => this.handleText(text, 2)}
              value={this.state.password}
            />
          </View>
        </View>
        <View style={styles.buttonView}>
          <TouchableOpacity style={styles.button} onPress={this.signInAction}>
            <Text style={styles.buttonText}>Log in</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colors.white,
  },
  headerView: {
    //backgroundColor: 'pink',
  },
  textFieldsView: {
    //backgroundColor: 'blue',
  },
  headerText: {
    color: colors.primary,
    fontFamily: fonts.primary_bold,
    fontSize: 35,
    //backgroundColor: 'teal',
    margin: '5%',
  },
  textField: {
    margin: '9%',
    marginVertical: '4%',
    height: 50,
    //backgroundColor: 'orange',
    borderWidth: 1,
    borderColor: colors.lineGray5,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    backgroundColor: colors.primary,
    marginVertical: '5%',
    width: '70%',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
  },
  buttonView: {
    //backgroundColor: 'yellow',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: colors.white,
    fontFamily: fonts.primary,
    fontSize: 20,
    marginVertical: '3%',
  },
  textInput: {
    width: '85%',
    height: '100%',
    fontFamily: fonts.primary,
    fontSize: 18,
  },
});
