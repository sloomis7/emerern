import React from 'react';
import Test1 from '../screens/template/TemplateScreen';
import HomeStack from '../navigation/HomeStack';
import FavoritesStack from '../navigation/FavoritesStack';
import SettingsStack from '../navigation/SettingsStack';

import {Image, StyleSheet, View} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

const Tab = createBottomTabNavigator();

export default function TabStack() {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;

          switch (route.name) {
            case 'HomeStack':
              iconName = focused
                ? require('../assets/tabBarIcons/home.png')
                : require('../assets/tabBarIcons/homeOff.png');
              break;
            case 'FavoritesStack':
              iconName = focused
                ? require('../assets/tabBarIcons/favorite.png')
                : require('../assets/tabBarIcons/favoriteOff.png');
              break;
            case 'SettingsStack':
              iconName = focused
                ? require('../assets/tabBarIcons/settings.png')
                : require('../assets/tabBarIcons/settingsOff.png');
              break;
            default:
              break;
          }
          if (route.name === 'HomeScreen') {
            iconName = focused
              ? require('../assets/tabBarIcons/home.png')
              : require('../assets/tabBarIcons/homeOff.png');
          } else if (route.name === 'Test1') {
            iconName = focused
              ? require('../assets/tabBarIcons/settings.png')
              : require('../assets/tabBarIcons/settingsOff.png');
          }

          // You can return any component that you like here!
          return <Image style={styles.logo} source={iconName} />;
        },
      })}
      tabBarOptions={{
        showLabel: false,
      }}>
      <Tab.Screen name="HomeStack" component={HomeStack} />
      <Tab.Screen name="FavoritesStack" component={FavoritesStack} />
      <Tab.Screen name="SettingsStack" component={SettingsStack} />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  logo: {
    width: 25,
    height: 25,
  },
});
