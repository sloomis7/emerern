import React from 'react';
import LoginScreen from '../screens/LoginScreen';
import SignUpScreen from '../screens/SignUpScreen';
import SignInScreen from '../screens/SignInScreen';
import TabStack from '../navigation/TabStack';
import {createStackNavigator} from '@react-navigation/stack';
import LeftButton from '../ui/navigation_bar/leftButton';
import ProductDetailsScreen from '../screens/ProductDetailsScreen';

import {Image, View} from 'react-native';
//import colors from '../config/colors';

const Stack = createStackNavigator();

export default function RootStack() {
  return (
    <Stack.Navigator mode="modal">
      <Stack.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{
          title: '',
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="SignUpScreen"
        component={SignUpScreen}
        options={({navigation}) => ({
          title: '',
          headerLeft: () => <LeftButton navigation={navigation} />,
        })}
      />
      <Stack.Screen
        name="SignInScreen"
        component={SignInScreen}
        options={({navigation}) => ({
          title: '',
          headerLeft: () => <LeftButton navigation={navigation} />,
        })}
      />
      <Stack.Screen
        name="TabStack"
        component={TabStack}
        options={({navigation}) => ({
          title: '',
          headerShown: false,
          animationEnabled: false,
        })}
      />
      <Stack.Screen
        name="ProductDetailsScreen"
        component={ProductDetailsScreen}
        options={({navigation}) => ({
          title: 'Product Details',
          headerShown: false,
        })}
      />
    </Stack.Navigator>
  );
}
