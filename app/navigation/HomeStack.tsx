import React from 'react';
import {Image, View} from 'react-native';

import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from '../screens/HomeScreen';
import RightButton from '../ui/navigation_bar/rightButton';
import ProductDetailsScreen from '../screens/ProductDetailsScreen';
import LeftButton from '../ui/navigation_bar/leftButton';

const Stack = createStackNavigator();

export default function HomeStack() {
  return (
    <Stack.Navigator mode="modal">
      <Stack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={({navigation}) => ({
          title: '',
          headerShown: true,
          headerLeft: () => null,
          headerRight: () => <RightButton navigation={navigation} />,
          headerTitle: () => (
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                width: '100%',
              }}>
              <Image
                style={{
                  width: 150,
                  height: 40,
                }}
                source={require('../assets/logo_name.png')}
                resizeMode="contain"
              />
            </View>
          ),
        })}
      />
      {/* <Stack.Screen
        name="ProductDetailsScreen"
        component={ProductDetailsScreen}
        options={({navigation}) => ({
          title: 'Product Details',
          headerLeft: () => <LeftButton navigation={navigation} />,
        })}
      /> */}
    </Stack.Navigator>
  );
}
