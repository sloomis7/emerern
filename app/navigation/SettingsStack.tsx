import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import Test1 from '../screens/template/TemplateScreen';

const Stack = createStackNavigator();

export default function SettingsStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="SettingsScreen"
        component={Test1}
        options={({navigation}) => ({
          title: 'Settings',
          headerShown: true,
          headerLeft: () => null,
        })}
      />
    </Stack.Navigator>
  );
}
