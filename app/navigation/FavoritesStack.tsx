import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import FavoritesScreen from '../screens/FavoritesScreen';

const Stack = createStackNavigator();

export default function FavoritesStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="FavoritesScreen"
        component={FavoritesScreen}
        options={({navigation}) => ({
          title: 'Favorites',
          headerShown: true,
          headerLeft: () => null,
        })}
      />
    </Stack.Navigator>
  );
}
