export default {
  signUp: 'api/signUp',
  signIn: 'api/signIn',
  login: 'api/login',
  getCategories: 'api/getCategories',
  getProductDetails: 'api/getProductDetails',
};
