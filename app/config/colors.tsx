export default {
  primary: '#ea4a6a',
  black: '#000',
  white: '#FFF',
  placeholderGray5: '#c4c4c6',
  placeholderGray4: '#dddddd',
  placeholderGray3: '#ededed',
  placeholderGray2: '#f8f8f8',
  lineGray5: '#959595',
  wordGray5: '#434343',
};
