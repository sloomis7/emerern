import {Component} from 'react';
import WsManager from '../singletons/WsManager';
import Product from '../models/Product';
import {Error} from '../util/globalEnum';

export default class ProductDetailsDC extends Component {
  constructor(props: any) {
    super(props);
    this.navProps = props;

    let {route}: any = props;
    let {productID} = route.params;
    this.productID = productID as string;
  }
  navProps: any;
  productID: string;

  product: Product | undefined;
  wsManager: WsManager = WsManager.getInstance();

  loadDetails(completion: (didSucceed: boolean, authFailed?: boolean) => void) {
    this.wsManager.getProductDetails(
      this.productID,
      (json: any, error?: Error) => {
        if (error != null) {
          if (error == Error.authFailed) {
            completion(false, true);
          } else {
            completion(false, false);
          }
          return;
        }
        let newProduct = Product.createProduct(json.product);

        if (newProduct != null) {
          this.product = newProduct;
          completion(true);
        } else {
          completion(false);
        }
      },
    );
  }
}
