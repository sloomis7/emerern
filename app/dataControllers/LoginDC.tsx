import {Component} from 'react';
import AppDebug from '../singletons/AppDebug';
import WsManager from '../singletons/WsManager';
import {getData} from '../util/storage';
import storageKeys from '../util/storage';
import User from '../models/User';

export default class LoginDC extends Component {
  appDebug: AppDebug = AppDebug.getInstance();

  wsManager: WsManager = WsManager.getInstance();
  user: User | undefined;

  isAutoTest() {
    if (this.appDebug.getAutoTest1() == true) {
      return true;
    }
    return false;
  }

  navigateToSignUp(props: any) {
    props.navigation.navigate('SignUpScreen');
  }

  navigateToSignIn(props: any) {
    props.navigation.navigate('SignInScreen');
  }
  navigateToHome(props: any) {
    props.navigation.navigate('TabStack');
  }

  login(completion: (didSucceed: boolean) => void) {
    getData(storageKeys.token, (token: string) => {
      if (token == null) {
        completion(false);
      } else {
        this.wsManager.loginUser(token, (json: any, error: any) => {
          if (error != null) {
            completion(false);
            return;
          }

          //TODO: make global
          this.user = User.createUser(json.user);

          if (this.user == null) {
            completion(false);
          } else {
            completion(true);
          }
        });
      }
    });
  }

  
}
