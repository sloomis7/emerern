import {Component} from 'react';
import WsManager from '../singletons/WsManager';
import Category, {Product} from '../models/Category';
import {Error} from '../util/globalEnum';

export default class HomeDC extends Component {
  constructor(props: any) {
    super(props);
    this.navProps = props;
  }
  navProps: any;

  wsManager: WsManager = WsManager.getInstance();

  emptyCategory = Category.createPlaceholderCategory('0');

  categories: Category[] | undefined;

  newArrivalCategory: Category | undefined;
  featuredCategory: Category | undefined;
  bestSellersCategory: Category | undefined;

  loadCategories(completion: (didSucceed: boolean, authFailed?: boolean) => void) {
    this.wsManager.getCategories((json: any, error: any) => {
      if (error != null) {
        if (error == Error.authFailed) {
          completion(false, true);
        } else {
          completion(false, false);
        }
        return;
      }

      let newCategories = Category.createCategories(json.categories);

      if (newCategories != null) {
        var finalCategories: Category[] = [];

        for (let category of newCategories!) {
          if (category.isFeatured) {
            switch (category.name) {
              case 'New Arrivals':
                this.newArrivalCategory = category;
                break;
              case 'Featured':
                this.featuredCategory = category;

                break;
              case 'Best Sellers':
                this.bestSellersCategory = category;

                break;
              default:
                break;
            }
          } else {
            finalCategories.push(category);
          }

          if (finalCategories.length > 0) {
            this.categories = finalCategories;
          }
        }

        completion(true);
      } else {
        completion(false);
      }
    });
  }

  didSelectProduct = (id: string) => {
    //console.log('did Select Product: ' + id);
    //test here...
    //how to pass props?
    //this.navProps.navigation.navigate('ProductDetailsScreen', {productID: id});

    
    this.navProps.navigation.navigate('ProductDetailsScreen', {
      productID: id,
    });
  };
}
