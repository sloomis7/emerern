import {Component} from 'react';
import AppDebug from '../singletons/AppDebug';
import WsManager from '../singletons/WsManager';
import {validateStringField} from '../util/jsonValidator';
import {storeData} from '../util/storage';
import storageKeys from '../util/storage';
import User from '../models/User';
import {StackActions} from '@react-navigation/native';

export default class SignUpDC extends Component {
  appDebug = AppDebug.getInstance();
  wsManager = WsManager.getInstance();

  isAutoTest() {
    if (this.appDebug.getSignUpAutoFill() == true) {
      return true;
    }
    return false;
  }

  navigateToHome(props: any) {
    props.navigation.dispatch(StackActions.replace('TabStack', {}));
  }

  signUp(
    state: any,
    completion: (didSucceed: boolean, message: string) => void,
  ) {
    this.wsManager.signUpUser(state, (json, error) => {
      if (validateStringField(json.message)) {
        completion(false, json.message);
        return;
      }

      if (validateStringField(json.token)) {
        storeData(storageKeys.token, json.token);
        this.wsManager.authHeader.Token = json.token
      } else {
        completion(false, '');
        return;
      }

      //TODO: make global
      let user = User.createUser(json.user);

      if (user == null) {
        completion(false, '');
      } else {
        completion(true, '');
      }
    });
  }

  testModelPersistence() {
    //Realm Save
    // insertNewUser(user)
    //   .then(() => {
    //     completion(true);
    //   })
    //   .catch((error) => {
    //     completion(false);
    //   });
    //Realm get
    // getUser(123)
    //   .then((user) => {
    //     console.log(user.name);
    //     completion(true);
    //   })
    //   .catch((error) => {
    //     completion(false);
    //   });
  }
}
