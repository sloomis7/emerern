module.exports = {
  root: true,
  extends: '@react-native-community',
  rules: {
    eqeqeq: 'off',
    react-native/no-inline-styles: 'off',
  },
};
